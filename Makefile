test: eseguibile
	./eseguibile -r junit > test-results.xml

eseguibile: test.o somma.o
	g++ -o eseguibile test.o somma.o --coverage

somma.o: somma.cpp somma.h
	g++ -c somma.cpp --coverage

test.o: test.cpp somma.h	
	g++ -c test.cpp