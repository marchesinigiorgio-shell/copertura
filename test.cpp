#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include "somma.h"

TEST_CASE("Somma"){

    int a = somma(2);

    REQUIRE(a == 2);
}